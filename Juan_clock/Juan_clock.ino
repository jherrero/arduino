/*-----( Import needed libraries )-----*/
//#include <Wire.h>  // Comes with Arduino IDE
// Get the LCD I2C Library here: 
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move any other LCD libraries to another folder or delete them
// See Library "Docs" folder for possible commands etc.
#include <LiquidCrystal_I2C.h>
#include <SFE_BMP180.h>
#include <DS3232RTC.h>        //http://github.com/JChristensen/DS3232RTC
#include <Streaming.h>        //http://arduiniana.org/libraries/streaming/
#include <Time.h>             //http://playground.arduino.cc/Code/Time
#include <Wire.h>             //http://arduino.cc/en/Reference/Wire


SFE_BMP180 bmp180;

#define ALTITUDE 30.5

double baseline, pressure, externalTemp, internalTemp; // baseline pressure


/*-----( Declare Constants )-----*/
//none
/*-----( Declare objects )-----*/
// set the LCD address to 0x20 for a 20 chars 4 line display
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
//LiquidCrystal_I2C lcd(0x20, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

/*-----( Declare Variables )-----*/
//none


void setup_lcd()   /*----( SETUP: RUNS ONCE )----*/
{
  lcd.begin(16,2);         // initialize the lcd for 20 chars 4 lines and turn on backlight

// ------- Quick 3 blinks of backlight  -------------
//  for(int i = 0; i< 3; i++)
//  {
//    lcd.backlight();
//    delay(250);
//    lcd.noBacklight();
//    delay(250);
//  }
  lcd.backlight(); // finish with backlight on  
  
  lcd.home();
  
//-------- Write characters on the display ----------------
// NOTE: Cursor Position: CHAR, LINE) start at 0  

//lcd.blink();
  lcd.setCursor(0,0); //Start at character 4 on line 0
  lcd.print("Hello, world!");
  //delay(1000);
  lcd.setCursor(0,1);
  lcd.print("16 x 2 display");
  delay(2000);   
  
// Wait and then tell user they can start the Serial Monitor and type in characters to
// Display. (Set Serial Monitor option to "No Line Ending")

  lcd.clear();
  lcd.setCursor(0,0); //Start at character 0 on line 0
  lcd.print("Real time clock");
  lcd.setCursor(0,1);
  lcd.print("version 0.1");   
  delay(2000);

}/*--(end setup )---*/

void setup_bmp180()
{
  lcd.clear();  
  if (bmp180.begin())
  {
    lcd.print("BMP180 ok");  
    //bmp180present = true;
    delay(2000);    
  }
  else
  {
    // Oops, something went wrong, this is usually a connection problem,
    // see the comments at the top of this sketch for the proper connections.

    lcd.print("BMP180 ERROR");

  }

}
void setup(void)
{
    Serial.begin(9600);
    
    setup_lcd();
    
    setup_bmp180();


    lcd.clear();
    
    setSyncProvider(RTC.get);   // the function to get the time from the RTC
    
    if(timeStatus() != timeSet) 
    {
        lcd.print("RTC sync ERROR");
    }
    else
    {
        lcd.print("RTC time read");        
    }

    lcd.home();        
    delay(4000);           
}




void loop(void)
{
    
    lcd.clear();    
    digitalClockDisplay();  
    delay(1000);
}

void digitalClockDisplay(void)
{
    // digital clock display of the time
   getBmp180Data();    
    
    lcd.print(hour());
    printDigits(minute());
    printDigits(second());
    lcd.print(' ');
    
    lcd.print(day());
    lcd.print('/');
    lcd.print(month());
    
   
    lcd.setCursor(0,1);       
    
    //print temperature
    internalTemp = RTC.temperature() / 4.;
    lcd.print(internalTemp,0);     
    lcd.print("/");      
    
    lcd.print(externalTemp,0);     
    lcd.print((char)0xDE);
    lcd.print("C - ");     
    
    lcd.print(pressure,0);     
    lcd.print("mb");     
       

  
    //lcd.println(); 
}

void printDigits(int digits)
{
    // utility function for digital clock display: prints preceding colon and leading 0
    lcd.print(':');
    if(digits < 10)
        lcd.print('0');
    lcd.print(digits);
}




void getBmp180Data()
{
  char status;
  double T,P,p0,a;

  // You must first get a temperature measurement to perform a pressure reading.
  
  // Start a temperature measurement:
  // If request is successful, the number of ms to wait is returned.
  // If request is unsuccessful, 0 is returned.

  status = bmp180.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:

    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Use '&T' to provide the address of T to the function.
    // Function returns 1 if successful, 0 if failure.

    status = bmp180.getTemperature(T);
    if (status != 0)
    {
      // Start a pressure measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.

      status = bmp180.startPressure(3);
      if (status != 0)
      {
        // Wait for the measurement to complete:
        delay(status);

        // Retrieve the completed pressure measurement:
        // Note that the measurement is stored in the variable P.
        // Use '&P' to provide the address of P.
        // Note also that the function requires the previous temperature measurement (T).
        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
        // Function returns 1 if successful, 0 if failure.

        status = bmp180.getPressure(P,T);
        if (status != 0)
        {
          pressure = bmp180.sealevel(P, ALTITUDE);
          externalTemp= T;
        }
        else Serial.println("error retrieving pressure measurement\n");
      }
      else Serial.println("error starting pressure measurement\n");
    }
    else Serial.println("error retrieving temperature measurement\n");
  }
  else Serial.println("error starting temperature measurement\n");
}


